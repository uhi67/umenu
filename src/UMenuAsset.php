<?php
namespace uhi67\umenu;

use yii\web\AssetBundle;

class UMenuAsset extends AssetBundle {
    public $sourcePath = '@vendor/uhi67/umenu/src/assets';
    public $css = [
        'umenu.css',
    ];
    public $js = [
    	'umenu.js',
	];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
	];
	public $publishOptions = [
		'forceCopy' => YII_ENV_DEV,
	];
}
