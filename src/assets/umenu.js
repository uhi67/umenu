/**
 * umenu.js
 * javascript support for UMenu extension
 * - gridview context-menu (bottom-menu is a style)
 * - gridview footer input form  
 */

console.log('umenu.js v1.0.1.2');

jQuery.fn.extend({
	// disables a button/menu item with style and click
	disable: function() {
		this.addClass('disabled');
		this.first().data('saved-click', this.get(0).onclick);
		this.get(0).onclick = null;
	},
	enable: function() {
		this.removeClass('disabled');
		this.get(0).onclick = this.data('saved-click');
	}
});

$(function() {
	// Disables hidden footer form at start
	let $trfoot = $('div.footer-form tfoot tr');
	$('input, select, textarea', $trfoot).attr('disabled', 'disabled');
	$trfoot.hide();
	
	// Dispose used item
	$('ul.umenu .disposable').on('click', function(){
		$(this).disable();
	});
	
	// Disabled menu items
	$('li.disabled a, dt.disabled a, a.disabled').each(function() {
		$(this).on('click', function() {return false;});
	});
	
	// Drop-down menu
	$('ul.dropdown').each(function() {
		let $submenu = $(this);
		//console.log('submenu: ',$submenu);
		$submenu.closest('li').on('hover',
			function(){
				console.log('hover: ',$submenu);
				if(!$(this).hasClass('disabled')) $submenu.show();
			}, 
			function(){$submenu.hide()}
		);
	});
	
	// Updates context-menu 'group' items on selection change
	let $grid = $('.grid.context-menu');
	$('input[name="selection_all"], input[name="selection[]"]', $grid).on('change', function() {
		let $grid = $(this).closest('.grid.context-menu');
		console.log('update contex-menu visibility');
		let keys = $grid.yiiGridView('getSelectedRows');
		let $groupitems = $grid.closest('form').find('.context-menu li.group');
		if(keys.length) $groupitems.show(); else $groupitems.hide();
	});
	
	// Group action on group button
	$('.context-menu li.group .menuitem').on('click', function() {
		console.log('group action');
		
		let keys = $grid.yiiGridView('getSelectedRows');
		let $form = $(this).closest('form');
		
		console.log('keys: '+keys + ' form:', $form);
		let action = $(this).data('action');
		if(action) $form.get(0).action = $form.get(0).action = action;
		
		let group = $(this).data('group');
		if(group) $('input.data-group').val(group);
		
		$form.trigger('submit');
	});
	
	// Open input row (gridview footer)
	$('.footer-form-show').on('click', function() {
		//console.log('show footer form');
		let $button = $(this);
		let $trfoot = $('.table tfoot tr', $button.closest('form'));
		$trfoot.show();
		// Enable all inputs in footer-form 
		$('input, select, textarea', $trfoot).removeAttr('disabled');
		
		$button.disable();
	});
	
	// Close input row (gridview footer)
	$('.footer-form-close').on('click', function() {
		let $trfoot = $(this).closest('tr');
		$trfoot.hide();
		let $button = $('.footer-form-show', $(this).closest('form').parent());
		$button.enable();
		// Disable all inputs to ensure to pass validation on footer-form 
		$('input, select, textarea', $trfoot).attr('disabled', 'disabled');
	});

	// Display badge on nav items
	$('ul.nav li a.badge-item').each(function() {
		let caption = $(this).data('badge-caption');
		$(this).after('<div class="badge-notify-container"><span class="badge badge-notify">'+caption+'</span></div>');
	});
});
